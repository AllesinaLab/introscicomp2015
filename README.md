# README #

Created by Stefano Allesina sallesina@uchicago.edu
for the course Introduction to Scientific Computing for Biologists (ECEV 32000). Academic Year 2014-2015.

The data comes from publicly available repositories, as detailed in the about.txt file associated with each data set.

The solutions for the exercises should be posted at 
http://introscicomp2015.wordpress.com/