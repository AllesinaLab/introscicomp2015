# This script takes in a tex file name, compiles it, and removes extra files
# example use: bash Compile.sh MyTexFile
# Notice that I say 'MyTexFile', not 'MyTexFile.tex'. The latter will not 
# compile correctly using this script.

pdflatex $1.tex
pdflatex $1.tex
bibtex $1
pdflatex $1.tex
pdflatex $1.tex
evince $1.pdf &

rm *.bbl
rm *~
rm *.blg
rm *.log
rm *.aux
rm *.toc

