import os     ## for terminal commands
import time   ## for timing submitting the jobs
import sys    ## for executing the script from terminal
import scipy  ## for reading in the matrix

def Launch(path, webname, initialrnd, nChains):
    ## path: path within Results folder where results go
    ## webname: file name for network
    ## initialrnd: random seed to pass to C
    ## nChains: another parameter to pass to C

    ## Hard coded parameters for beast
    ## Always keep your hard coded parameters
    ## at the top of the file where they are easy
    ## to find!
    ## Number of runs to pack in each shell script
    TOPACK = 1
    ## Total number of jobs to submit
    nruns = 25
    ## Fixed parameters to pass to C
    Steps = 10000
    logYN = 0
    ## Which beast queue to submit to
    ## If this isn't 'batch', the python script will
    ## not submit the .sh files. This is good for testing.
    Queue = 'batch'
    
    
    ## move to CCode directory to compile code
    os.chdir("../CCode/FindGroupGibbs")
    os.system("make clean")
    os.system("make")
    ## move to directory where we want to put the results
    os.chdir("../../Results/" + path)

    ## read in network
    ## find how many species are there
    z = scipy.genfromtxt(webname)
    numspp = z.shape[0]

    currnd = int(initialrnd)
    ## we will use this number so that the shell scripts 
    ## all have different names
    numscript = 0

    ## loop over the total number of jobs (scripts)
    for run in xrange(nruns):
        ## give each job a standard name
        jobname = webname + "-MC3-" + str(numscript) + ".sh"
        ## open the shell script file for writing
        f = open(jobname, "w")
        ## this is information that the grid needs to process my job
        ## syntax will differ depending on your computing cluster
        f.write("#!/bin/bash\n#PBS -N %s \n#PBS -q %s\n#PBS -d . \n#PBS -j oe \n#PBS -S /bin/bash \n" % (jobname, Queue))
        
        ## loop over the number of runs to put in each job
        for i in range(TOPACK):
            ## this line executes my C code
            f.write("../../../CCode/FindGroupGibbs/FindGroupsGibbs " + str(numspp) + " " + str(numspp) + " " + webname + " " + str(currnd) + " 0 " + str(Steps) + " " + str(nChains) + " " + str(logYN) + "\n")
            ## update my random seed so we don't use the 
            ## same one over and over
            currnd = currnd + 1

        ## close our file
        f.close()
        numscript = numscript + 1
            
        ## launch the scripts
        if Queue == 'batch':
            ## this syntax is specific to the E&E grid
            os.system('qsub ' + jobname)
            ## and wait a second
            ## this is so that we don't overload the head node
            ## if you don't include this, your IT guy may get very angry
            time.sleep(1)

## this lets us call the python script from the terminal
if __name__ == "__main__":
    path = sys.argv[1]
    webname = sys.argv[2]
    initialrnd = sys.argv[3]
    nChains = sys.argv[4]
    Launch(path, webname, initialrnd, nChains)
