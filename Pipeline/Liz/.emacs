;; .emacs file to configure emacs on boot
;; keep this file in your home directory so emacs can find it!

;; Color the code
(require 'font-lock)
(setq-default font-lock-maximum-decoration t)
(setq-default search-highlight t)
(setq-default query-replace-highlight t)
(global-font-lock-mode t)

;; Set the starting appearance
;; uncomment to get Stefano's preferred color scheme
;; you will also need to comment out the lines to load zenburn
;;(setq default-frame-alist
      ;;'(;;(cursor-color . "Firebrick")
        ;;(cursor-color . "White")
        ;;(cursor-type . box)
        ;;(foreground-color . "White")
        ;;(background-color . "DarkSlateGray")
        ;;(foreground-color . "White")
        ;;(background-color . "Black")
        ;;(vertical-scroll-bars . right)))

;; set up color theme
;; to get this working, download this file from github:
;; https://github.com/djcb/elisp/blob/master/themes/zenburn-theme.el
;; (right-click Raw, then 'Save Link As' to download it)
;; save the file in ~/.emacs.d/themes/
;; (create that folder if it doesn't already exist)
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(load-theme 'zenburn t)

;;suppress help messages when emacs loads
(setq inhibit-startup-echo-area-message t)
(setq inhibit-startup-message t)

;; ---   Parenthesis matches
;; Uncomment these lines if you want this feature
;;
;;(when (try-require 'paren)
;;    (progn
;;      (show-paren-mode t)
;;      (setq show-paren-delay 0)))

(set-default-font "Monospace-12")

;;ESS: Emacs Speaks Statistics
;;automatically loads the .el file so you don't need to install anything
(load "~/ess-13.09-1/lisp/ess-site.el")
(setq inferior-ess-same-window nil)
;;all help buffers go into one frame
(setq ess-help-own-frame 'one)
;;have R start in current working directory by default
(setq ess-ask-for-ess-directory nil)
(setq ess-local-process-name "R")
;; have "Shift+Enter" to send current line to R
(defun my-ess-eval ()
(interactive)
(if (and transient-mark-mode mark-active)
(call-interactively 'ess-eval-region)
(call-interactively 'ess-eval-line-and-step)))
(add-hook 'ess-mode-hook
'(lambda()
(local-set-key [(shift return)] 'my-ess-eval)))

;; ---   C
;;
(add-hook 'c-mode-common-hook
          (lambda ()
            (message "Setting up my C hooks")
            (setq c-basic-offset 4)
            (setq tab-width 4)
            (setq c-default-style "K&R") ; gnu k&r bsd stroustrup linux
                                        ; python java
            (auto-fill-mode t)
            (local-set-key [return] 'newline-and-indent)
            (hs-minor-mode t)
            (local-set-key "\C-c\C-q" 'comment-region) ;; c indent-region
            (my-auto-load-tags-file)
            ;(global-set-key "(" 'skeleton-pair-insert-maybe)
            ;(global-set-key "[" 'skeleton-pair-insert-maybe)
            ;(global-set-key "{" 'skeleton-pair-insert-maybe)
            ;(global-set-key "\"" 'skeleton-pair-insert-maybe)
            ;(global-set-key "'" 'skeleton-pair-insert-maybe)
            ))

;; ---   LaTeX & TeX
;;
(add-hook 'latex-mode-hook
          (lambda ()
            (message "Setting up my LaTeX hooks")
            ;;(setq tab-width 4)
            (auto-fill-mode t)
            (local-set-key [return] 'newline-and-indent)
            ;;(local-set-key "\C-c\C-q" 'TeX-comment-or-uncomment-region)
            (setq TeX-auto-save t)
            (setq TeX-parse-self t)
            (setq-default TeX-master nil)
            (setq TeX-auto-untabify t)
            ;; http://staff.science.uva.nl/~dominik/Tools/reftex/reftex.html
            (autoload 'reftex "reftex"
                  "Mode for managing Labels, References, Citations and index entries" t)          
            (reftex-mode t)
            ;;(flyspell-mode t)
            (setq reftex-plug-into-AUCTeX t)
            (setq reftex-use-external-file-finders t)
            (local-set-key "\C-c\C-q" 'comment-region)
            ))

;; ---   Markdown
;; Markdown is a markup language like a simpler version of LaTeX
;; I use it for bitbucket README.md files. Bitbucket will 
;; automatically compile and display them nicely in the repo online.x
(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))



;;(add-hook 'after-save-hook 'autocompile)

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(show-paren-mode t)
 '(text-mode-hook (quote (turn-on-auto-fill text-mode-hook-identify))))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )


;;stuff taken from init file in pj.freefaculty.org/guides/Rcourse/emacs-ess/emacs-ess.pdf

;;Make emacs scroll smoothly with down arrow key
(setq scroll-conservatively most-positive-fixnum)

;;Make emacs shells work better
(setq ansi-color-for-comint-mode 'filter)
(setq comint-prompt-read-only t)
(setq comint-scroll-to-bottom-on-input t)
(setq coming-scroll-to-bottom-on-output t)
(setq comint-move-point-for-output t)
