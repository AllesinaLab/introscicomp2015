Data taken from the Dryad data package:

Grimm A, Prieto Ramírez AM, Moulherat S, Reynaud J, Henle K (2014) Data from: Life-history trait database of European reptile species. Dryad Digital Repository. http://dx.doi.org/10.5061/dryad.hb4ht

Associated with the publication:

Grimm A, Prieto Ramírez AM, Moulherat S, Reynaud J, Henle K (2014) Life-history trait database of European reptile species. Nature Conservation 9: 45-67. http://dx.doi.org/10.3897/natureconservation.9.8908


